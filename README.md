# Star Wars Top Trump

## Introduction

The front-end code for the Star Wars Top Trump Game. Built in [React](https://reactjs.org/).

## Requirements

- [Node](https://nodejs.org/en/).
- [npm](https://www.npmjs.com/package/npm).

## Getting Started

**1. Clone Git Repo.**

```
$ git clone https://kkbokyere@bitbucket.org/kkbokyere/star-wars-trump.git
```

**2. Install Dependencies.**

Once that's all done, cd into the star-wars-trump directory and install the depedencies:

```
$ cd star-wars-trump/web
$ yarn install
```

**3. Run Application.**

Once the node modules have all been installed and npm has done it's thing, that's it. To open up a local development environment, run:

```
$ yarn start
```

Once the server is up and running, navigate to [localhost:3000](http://localhost:3000).

## Testing

[Jest](https://jestjs.io/) is the test runner used, with [React Testing Library](https://testing-library.com/docs/react-testing-library/) is testing library used for testing components. To run test use the following command:

```
$ yarn test
```

## Deployment

No CI/CD pipeline at the moment.

# Tools Used

- [React](https://reactjs.org/)
- [Create React App](https://create-react-app.dev/)
- [Redux](https://redux.js.org)
- [Typescript](https://typescriptlang.org/)
- [Apollo](https://www.apollographql.com/docs/react/)
- [Redux Saga](https://github.com/redux-saga/redux-saga)
- [Styled Components](https://styled-components.com/)
- [Webpack](https://webpack.js.org/)

# Improvements / Retrospective Review

- Would consider using a HOC component to pass the apollo queries
- Would have used Cypress for E2E testing
- 100% test coverage
- Find a better way to randonmize the People and Starships. Can this be passed onto the GraphQL API backend? or is there a better way to write a query or some mutation.
- create a better Error handler for Apollo data
- create more reusable generic types
- add winner flag to the history list
- better typescript definitions
- use graphql.macro to load in the graphql queries
- a full E2E test of connected component (for some reason data wasn't being passed to children in test helper render function)
- Would have considered creating a proper [SMACSS](http://smacss.com/) architecture for base CSS styles such as layout. 
