import React, {ReactNode} from 'react';
import { Button } from './Button.style';

interface IProps {
    handleOnClick?: (event: React.MouseEvent<HTMLButtonElement>) => void,
    children?: ReactNode | string
}
export default ({ handleOnClick, children} : IProps) => {
    return(
        <Button onClick={handleOnClick}>
            {children}
        </Button>
    )
};
