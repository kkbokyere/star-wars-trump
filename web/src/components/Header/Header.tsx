import React from "react";
import {
    NavLink
} from 'react-router-dom';
import {HISTORY, HOME} from "../../constants/routes";
const Header = () => (
    <header>
        <NavLink to={HOME}>Home</NavLink>
        <NavLink to={HISTORY}>History</NavLink>
    </header>
);

export default Header;
