import styled from "styled-components";

export const Card = styled.div`
border: 1px solid grey;
border-radius: 3px;
padding: 10px;
background: ${props => props.highlight ? "green" : "white"};
color: ${props => props.highlight ? "white" : "#000"}
`;
