import React from 'react';
import { Card } from './Card.style';

interface Props {
    data?: {[index: string]:any},
    isHighlighted?: boolean
}
export default ({ data, isHighlighted } : Props) => {
    if(!data)
    {
        return null
    }
    const { name } = data;
    return(
        <Card highlight={isHighlighted}>
            <div>{name}</div>
            <ul>
                {data && Object.keys(data).map((val) => <li key={val}>{val} : {data[val]}</li>)}
            </ul>
        </Card>
    )
};
