import React, {useEffect, useState} from "react";
import Button from '../../components/Button/Button';
import CardContainer from '../../containers/CardContainer';
import {selectRandomItem} from "../../utils/utils";
interface IProps {
    data: Array<Object>,
    handleOnSelectCardType: (selection: string) => void,
    handleAddToHistory: (selection: any) => void,
}
const Home = ({ data, handleOnSelectCardType, handleAddToHistory } : IProps) => {
    const [userSelection, setUserSelection] = useState();
    const [compSelection, setCompSelection] = useState();
    const [isUserWinner, setIsUserWinner] = useState(false);
    const userCardData = userSelection && selectRandomItem(data[userSelection]);
    const compCardData = compSelection && selectRandomItem(data[compSelection]);
    const handleOnClickSelection = (selection: string) => {
        setUserSelection(selection);
        //TODO: find a better way to do this
        setCompSelection(selection === 'allPersons' ? 'allStarships' : 'allPersons' );
        handleOnSelectCardType(selection);
        return false;
    };
    const handleClickReset = () => {
        handleOnClickSelection('');
        setIsUserWinner(false)
    };
    useEffect(() => {
        if(userSelection) {
        setIsUserWinner(Math.floor(userCardData.value) > Math.floor(compCardData.value));
        handleAddToHistory([userCardData, compCardData]);
        }
    }, [userCardData, compCardData, handleAddToHistory, userSelection]);
    if(!userSelection) {
        return (
            <div>
                <header>
                    <h2>Pick Your Card</h2>
                </header>
                <section>
                    <Button handleOnClick={() => handleOnClickSelection('allPersons')}>People</Button>
                    <Button handleOnClick={() => handleOnClickSelection('allStarships')}>Starships</Button>
                </section>
            </div>
        )
    }
    return (
        <div>
            <h3>You Selected: {userSelection}</h3>
            <h4>You {isUserWinner ? 'WON!' : 'LOST :('} this round</h4>
            <section>
                <CardContainer userSelection={userCardData} compSelection={compCardData} isUserWinner={isUserWinner}/>
            </section>
            <Button handleOnClick={() => handleClickReset()}>Reset</Button>
        </div>
    );
};

export default Home;
