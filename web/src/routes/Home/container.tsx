import React from "react";
import { connect } from "react-redux";
import {useQuery} from "@apollo/client";
import {GET_ALL_DATA} from "../../constants/queries";
import Home from "./Home";
import {selectCardType} from "../../actions/user";
import {addHistory} from "../../actions/history";

interface StateFromProps {
    user: {[index: string]:any}
}

interface DispatchFromProps {
    handleOnSelectCardType: () => void
}

const mapStateToProps = (state: StateFromProps) => {
    return {
        user: state.user
    }
};

const mapDispatchToProps = (dispatch: DispatchFromProps)  => {
    return {
        handleOnSelectCardType: (type: string) => {
            // @ts-ignore
            dispatch(selectCardType(type))
        },
        handleAddToHistory: (card: object) => {
            // @ts-ignore
            dispatch(addHistory(card))
        }
    }
};

export default connect<StateFromProps, DispatchFromProps, void>(
    // @ts-ignore
    mapStateToProps,
    mapDispatchToProps) ((props: any) => {
    const { loading, error, data } = useQuery(GET_ALL_DATA);
    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error loading data, please try again:(</p>;
    return <Home data={data} {...props}/>
})

