import React, {Fragment} from "react";
interface IProps {
    cardHistory: Array<any>
}
const History = ({ cardHistory }:IProps) => {
    return(
        <div>
            <h2>History</h2>
            {cardHistory.map((round, index) => {
                return(
                    <li key={index}>Round {index+1}
                    <ul>
                        {round.map((results: any) => {
                            return(
                                <Fragment>
                                    <li key={results.id}>{results.name} | {results.value}</li>
                                </Fragment>
                            )})}
                    </ul>
                </li>)
            })}
        </div>
    );
};

export default History;
