import { connect } from "react-redux";
import History from "./History";

interface StateFromProps {
    history: {[index: string]:any}
}

const mapStateToProps = (state: StateFromProps) => {
    return {
        cardHistory: state.history
    }
};

export default connect<StateFromProps, void, void>(
    // @ts-ignore
    mapStateToProps) (History)

