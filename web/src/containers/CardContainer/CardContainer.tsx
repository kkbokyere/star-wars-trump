import React, { Fragment } from "react";
import Card from "../../components/Card";
interface Props {
    userSelection?: {[key: string]:any},
    compSelection?: {[key: string]:any},
    data?: object
    isUserWinner?: boolean
}
const CardContainer = ({ userSelection, compSelection, isUserWinner }: Props) => {
    return (
        (<Fragment>
            <h3>User Selection</h3>
            <Card key={'user-card'} data={userSelection} isHighlighted={isUserWinner}/>

            <h3>Computer Selection</h3>
            <Card key={'comp-card'} data={compSelection} isHighlighted={!isUserWinner}/>
        </Fragment>)
    );
};

export default CardContainer;
