import {ADD_HISTORY} from "../constants/action-types/history";

export function addHistory(payload: any) {
    return {
        type: ADD_HISTORY,
        payload
    }
}

