import {SELECT_CARD_TYPE} from "../constants/action-types/user";

export function selectCardType(payload: string) {
    return {
        type: SELECT_CARD_TYPE,
        payload
    }
}

export default {}
