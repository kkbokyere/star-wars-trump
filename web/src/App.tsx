import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';
import './styles/App.css';
import History from './routes/History';
import Home from './routes/Home';
import {HISTORY, HOME} from "./constants/routes";
import Header from "./components/Header";

const App = () => {
  return(
      <Router>
        <Header/>
        <Switch>
          <Route path={HOME} exact component={Home}/>
          <Route path={HISTORY} exact component={History}/>
        </Switch>
      </Router>
  )
};

export default App;
