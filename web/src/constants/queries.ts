import { gql } from '@apollo/client'

export const GET_PEOPLE = gql`
    query getPeople{
        allPersons {
            name
            height
            id
        }
    }`;

export const GET_STARSHIPS = gql`
    query getStarships{
        allStarships{
            hyperdriveRating
            name
            id
        }
    }`;

export const GET_ALL_DATA = gql`
    query getData{
        allStarships{
            value: hyperdriveRating
            name
            id
        }
        allPersons {
            value: height
            name
            id
        }
    }`;
