import * as user from './user'
import * as history from './history'

export default {
    ...user,
    ...history
}
