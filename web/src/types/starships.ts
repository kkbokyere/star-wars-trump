export default interface Starships {
    name: string,
    hyperdriveRating: string,
    [key:string]:any
}
