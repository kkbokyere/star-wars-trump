export default interface Person {
    name: string,
    height: number,
    [key:string]:any
}
