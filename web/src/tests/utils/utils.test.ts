import {selectRandomItem} from "../../utils/utils";

describe('Utils', () => {
    test('should pick a random item', async () => {
        expect(selectRandomItem(['a','b','c'])).toMatch(/^a|b|c$/)
    });
});
