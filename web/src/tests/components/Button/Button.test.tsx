import React from 'react';
import renderer from 'react-test-renderer';
import Button from '../../../components/Button';

describe('Button', () => {
    test('should render Button with text',  () => {
        const tree = renderer.create(<Button>Test</Button>).toJSON();
        expect(tree).toMatchSnapshot();
    });
    test('should render empty button',  () => {
        const tree = renderer.create(<Button/>).toJSON();
        expect(tree).toMatchSnapshot();
    });
});
