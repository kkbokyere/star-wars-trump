import React from 'react';
import renderer from 'react-test-renderer';
import {render } from "../../helper";

import Card from '../../../components/Card';

describe('Card', () => {
    const cardData = {
        name: 'Testing Name',
        id: '1',
        height: 20
    };
    test('should render empty card',  () => {
        const tree = renderer.create(<Card data={{}}/>).toJSON();
        expect(tree).toMatchSnapshot();
    });

    test('should render card with data',  () => {
        const tree = renderer.create(<Card data={cardData}/>).toJSON();
        expect(tree).toMatchSnapshot();
    });

    test('should render card with highlighted prop',  () => {
        const { container } = render(<Card data={cardData} isHighlighted/>);
        expect(container.firstChild).toHaveStyle(`background: green`);
        expect(container.firstChild).toHaveStyle(`color: white`)
    });
});
