import React from 'react';
import { render } from '../../helper'
import Header from "../../../components/Header";

describe('Header', () => {
    const { getByText } = render(<Header/>);
    test('should render Header',  () => {
        const homeLink = getByText('Home');
        const historyLink = getByText('History');
        expect(homeLink).toBeInTheDocument();
        expect(historyLink).toBeInTheDocument();
    });
});
