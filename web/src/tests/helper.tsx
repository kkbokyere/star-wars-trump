// @ts-nocheck
import * as React from "react";
import {createStore} from 'redux'
import {Provider} from 'react-redux'
import {render as rtlRender } from '@testing-library/react'
import { MockedProvider } from "@apollo/client/testing";
import { MemoryRouter } from "react-router-dom";

import rootReducer from '../reducers'
import {GET_ALL_DATA} from "../constants/queries";

interface IProps {
    children: any
}
const initialReducerState = {};

export const mocks = [
    {
        request: {
            query: GET_ALL_DATA,
        },
        result: {
            data: {
                allPersons: [{
                    "name": "Luke Skywalker",
                    "height": 172,
                    "id": "cj0nv9p8yewci0130wjy4o5fa"
                }],
                allStarships: [{
                    "hyperdriveRating": 4,
                    "name": "Death Star",
                    "id": "cj0nwtqphq4tr0114zo7suw8h"
                }],
            },
        },
    },
];

export function render(
    ui,
    {
        initialState = initialReducerState,
        store = createStore(rootReducer, initialState),
        ...renderOptions
    } = {},
) {
    function Wrapper({children}: IProps) {
        // eslint-disable-next-line react/react-in-jsx-scope
        return (
            <MockedProvider mocks={mocks}>
                <Provider store={store}>
                    <MemoryRouter>
                    {children}
                    </MemoryRouter>
                </Provider>
            </MockedProvider>
            )
    }
    return rtlRender(ui, {wrapper: Wrapper, ...renderOptions})
}
