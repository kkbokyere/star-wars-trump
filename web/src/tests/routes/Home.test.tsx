import React from 'react';
import { render, mocks } from '../helper'
import { waitFor, fireEvent, screen } from '@testing-library/react'

import Home from "../../routes/Home";

describe('Home', () => {
    test('should render homepage', async () => {
        render(<Home data={mocks[0].result.data} />);

        const peopleButton = await screen.findByText('People');
        const starShipsButton = await screen.findByText('Starships');
        const title = await screen.findByText('Pick Your Card');
        expect(title).toBeInTheDocument();
        expect(peopleButton).toBeInTheDocument();
        expect(starShipsButton).toBeInTheDocument();

    });

    test('should render select winner', async () => {
        render(<Home data={mocks[0].result.data} />);

        const peopleButton = await screen.findByText('People');
        fireEvent.click(peopleButton);
        screen.findByText('You Selected: allPersons')
    });
});
