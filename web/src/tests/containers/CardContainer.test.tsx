import React from 'react';
import renderer from 'react-test-renderer';
import CardContainer from '../../containers/CardContainer';
describe('CardContainer', () => {
    const userSelectionMock = {
        value: 1.5,
        name: "Trade Federation cruiser",
        id: "cj0nwtqytq5800114zjhy9gcj"
    };

    const compSelectionMock = {
        value: 165,
        name: "Padmé Amidala",
        id: "cj0nv9q0eewxh0130orm19nl4"
    };

    test('should render empty card container',  () => {
        const tree = renderer.create(<CardContainer />).toJSON();
        expect(tree).toMatchSnapshot();
    });
    test('should render cards with one highlighted',  () => {
        const tree = renderer.create(<CardContainer userSelection={userSelectionMock} compSelection={compSelectionMock} isUserWinner={true}/>).toJSON();
        expect(tree).toMatchSnapshot();
    });
});
