import React from 'react';
import { render } from './helper'
import App from '../App';

describe('Initial App render', () => {

    test('should render homepage', async () => {
        const { asFragment } = render(<App />, {
            initialState: {
                history: [],
                user: {
                }
            }
        });
        expect(asFragment()).toMatchSnapshot();
    });
    test('should render homepage', async () => {
        const { findByText } = render(<App />);
        const title = await findByText('Pick Your Card');
        const peopleButton = await findByText('People');
        const starShipsButton = await findByText('Starships');
        const homeLink = await findByText('Home');
        const historyLink = await findByText('History');

        expect(title).toBeInTheDocument();
        expect(peopleButton).toBeInTheDocument();
        expect(starShipsButton).toBeInTheDocument();
        expect(historyLink).toBeInTheDocument();
        expect(homeLink).toBeInTheDocument();
    });
});
