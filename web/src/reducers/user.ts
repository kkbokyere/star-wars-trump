import types from '../constants/action-types';

type ActionType = {
    payload?: any,
    type: string
}

type TInitialState = {
    cardType: string
}

const initialState: TInitialState = {
    cardType: ''
};

export default (state = initialState, action: ActionType) => {
    let { payload } = action;

    switch (action.type) {
        case types.SELECT_CARD_TYPE:
            return {
                ...state,
                cardType: payload
            };
        default:
            return state
    }
}
