import types from '../constants/action-types';

type ActionType = {
    payload?: any,
    type: string
}

const initialState: Array<any> = [];

export default (state = initialState, action: ActionType) => {
    let { payload } = action;

    switch (action.type) {
        case types.ADD_HISTORY:
            return [...state, payload];
        default:
            return state
    }
}
