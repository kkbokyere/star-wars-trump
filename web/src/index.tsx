import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import rootReducer from './reducers'
import { createStore } from 'redux'
import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client';
import { composeWithDevTools } from 'redux-devtools-extension';

import './styles/index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {GRAPH_QL_SERVER} from './constants/api';

// Redux store
const store = createStore(rootReducer, composeWithDevTools());
//Init the client with the HttpLink and some additional props
const client = new ApolloClient({
    //httpLink to connect Apollo with GraphQLApi
    uri: GRAPH_QL_SERVER,
    cache: new InMemoryCache({
        addTypename: false
    })
});

ReactDOM.render(
  <React.StrictMode>
      <ApolloProvider client={client}>
          <Provider store={store}>
              <App />
          </Provider>
      </ApolloProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
